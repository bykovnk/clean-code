# Чистый код Python

## Что обсудим

  1. [Введение](#введение)
  2. [Переменные](#переменные)
  3. [Функции](#функции)
  4. [Классы](#классы)
     * S: Принцип единой ответственности (Single Responsibility Principle - SRP)
     * O: Принцип открытости/закрытости (Open/Closed Principle - OCP)
     * L: Принцип подстановки Барбары Лисков (Liskov Substitution Principle - LSP)
     * I: Принцип разделения интерфейсов (Interface Segregation Principle - ISP)]
     * D: Принцип инверсии зависимостей (Dependency Inversion Principle - DIP)
  5. [Don’t repeat yourself (DRY)](#dont-repeat-yourself-dry)

## Введение

<img src="./img/cc.jpg" alt="Чистый код" width="200"/>
<img src="./img/ca.jpg" alt="Чистый архитектура" width="186"/>

## **Переменные**

### Используйте осмысленные и произносимые имена переменных

<span style="color:red">**Bad**</span>

```python
ymdstr = datetime.date.today().strftime("%y-%m-%d")
```

<span style="color:green">**Good**</span>

```python
current_date = datetime.date.today().strftime("%y-%m-%d")
```

### Используйте одни и те же названия для одной и той же категории переменных

<span style="color:red">**Bad**</span>

```python
def get_user_info(): pass
def get_client_data(): pass
def get_customer_record(): pass
```

<span style="color:green">**Good**</span>

```python
def get_user_info(): pass
def get_user_data(): pass
def get_user_record(): pass
```

### Используйте названия, которые просто искать

<span style="color:red">**Bad**</span>

```python
# What the heck is 86400 for?
time.sleep(86400);

```

<span style="color:green">**Good**</span>

```python
# Declare them in the global namespace for the module.
SECONDS_IN_A_DAY = 60 * 60 * 24

time.sleep(SECONDS_IN_A_DAY)
```

### Используйте переменные для пояснения контекста (explanatory variables)

<span style="color:red">**Bad**</span>

```python
import re

address = "One Infinite Loop, Cupertino 95014"
city_zip_code_regex = r"^[^,\\]+[,\\\s]+(.+?)\s*(\d{5})?$"

matches = re.match(city_zip_code_regex, address)
if matches:
    print(f"{matches[1]}: {matches[2]}")
```

<span style="color:olivedrab">**Not Bad**</span>

Лучше, но сильно зависит от regex.

```python
import re

address = "One Infinite Loop, Cupertino 95014"
city_zip_code_regex = r"^[^,\\]+[,\\\s]+(.+?)\s*(\d{5})?$"
matches = re.match(city_zip_code_regex, address)

if matches:
    city, zip_code = matches.groups()
    print(f"{city}: {zip_code}")
```

<span style="color:green">**Good**</span>

Снизили зависимость от regex именуя сабпаттерны.

```python
import re

address = "One Infinite Loop, Cupertino 95014"
city_zip_code_regex = r"^[^,\\]+[,\\\s]+(?P<city>.+?)\s*(?P<zip_code>\d{5})?$"

matches = re.match(city_zip_code_regex, address)
if matches:
    print(f"{matches['city']}, {matches['zip_code']}")
```

### Не полагайтесь на память читающего

Явное лучше чем неявное.

<span style="color:red">**Bad**</span>

```python
seq = ('Austin', 'New York', 'San Francisco')

for item in seq:
    do_stuff()
    do_some_other_stuff()
    # ...
    # Wait, what's `item` for again?
    dispatch(item)
```

<span style="color:green">**Good**</span>

```python
locations = ('Austin', 'New York', 'San Francisco')

for location in locations:
    do_stuff()
    do_some_other_stuff()
    # ...
    dispatch(location)
```

### Не добавляйте лишний текст к названию


Если имя вашего класса / объекта вам что-то говорит, не повторяйте это в своем
имя переменной. 

<span style="color:red">**Bad**</span>

```python
class Car:
    car_make: str
    car_model: str
    car_color: str
```

<span style="color:green">**Good**</span>

```python
class Car:
    make: str
    model: str
    color: str
```

### Используйте переменные по умолчанию вместо проверки условий

<span style="color:maroon">**Not Good**</span>

not good потому что `brewery_name` может быть `None`.

```python
def create_microbrewery(brewery_name = 'Hipster Brew Co.'):
    pass
```

<span style="color:olivedrab">**Not Bad**</span>

Лучше чем предыдущий вариант, но явной проверки на `None` все же можно избежать.

```python
def create_micro_brewery(name):
    name = "Hipster Brew Co." if name is None else name
```

<span style="color:green">**Good**</span>

```python
from typing import Text

def create_micro_brewery(brewery_name: Text = 'Hipster Brew Co.'):
    pass
```

## **Функции**

### Число аргументов (2 или меньше)

Ограничение количества параметров функции невероятно важно, потому что делает
тестирование функции значительно легче. Наличие более трех аргументов приводит к combinatorial explosion,
где нужно протестировать множество разных случаев с каждым отдельным аргументом.

Отсутсвтие аргументов - идеальный случай. Один или два аргумента - это нормально, трех следует избегать.
Все, что больше этого, следует консолидировать. Обычно, если у вас больше двух
аргументов, то ваша функция пытается сделать слишком много.

<span style="color:red">**Bad**</span>

```python
def create_menu(title, body, button_text, cancellable):
    pass
```

<span style="color:green">**Good**</span>

```python
from typing import Text


class MenuConfig:
    """A configuration for the Menu.

    Attributes:
        title: The title of the Menu.
        body: The body of the Menu.
        button_text: The text for the button label.
        cancellable: Can it be cancelled?
    """
    title: Text
    body: Text
    button_text: Text
    cancellable: bool = False


def create_menu(config: MenuConfig) -> None:
    title = config.title
    body = config.body
    # ...


config = MenuConfig()
config.title = "My delicious menu"
config.body = "A description of the various items on the menu"
config.button_text = "Order now!"
# The instance attribute overrides the default class attribute.
config.cancellable = True

create_menu(config)
```

### Функции должны совершать только одно действие

Это, безусловно, самое важное правило в разработке программного обеспечения. Когда функции делают больше
чем что-то одно, их сложнее сочинить, протестировать и обдумать. Когда можно изолировать
функция только для одного действия, их можно легко реорганизовать, и ваш код будет читаться существенно чище. Если вы не вынесете из этого руководства ничего другого, кроме этого, вы уже будете писать код лучше большинства разработчиков.

<span style="color:red">**Bad**</span>

```python
from typing import List


class Client:
    active: bool


def email(client: Client) -> None:
    pass


def email_clients(clients: List[Client]) -> None:
    """Filter active clients and send them an email.
    """
    for client in clients:
        if client.active:
            email(client)
```

<span style="color:green">**Good**</span>

```python
from typing import List


class Client:
    active: bool


def email(client: Client) -> None:
    pass


def get_active_clients(clients: List[Client]) -> List[Client]:
    """Filter active clients.
    """
    return [client for client in clients if client.active]


def email_clients(clients: List[Client]) -> None:
    """Send an email to a given list of clients.
    """
    for client in get_active_clients(clients):
        email(client)
```

### Названия функций должны быть содержательными

<span style="color:red">**Bad**</span>

```python
class Email:
    def handle(self) -> None:
        pass

message = Email()
# What is this supposed to do again?
message.handle()
```

<span style="color:green">**Good**</span>

```python
class Email:
    def send(self) -> None:
        """Send this message"""

message = Email()
message.send()
```

### Функции внутри должны придерживаться одного уровня абстракций

Когда у вас более одного уровня абстракции, ваша функция обычно делает слишком много. Разделение функций приводит к повторному использованию и упрощению
тестирования.

<span style="color:red">**Bad**</span>

```python
def parse_better_js_alternative(code: str) -> None:
    regexes = [
        # ...
    ]

    statements = code.split('\n')
    tokens = []
    for regex in regexes:
        for statement in statements:
            pass

    ast = []
    for token in tokens:
        pass

    for node in ast:
        pass
```

<span style="color:green">**Good**</span>

```python
from typing import Tuple, List, Text, Dict


REGEXES: Tuple = (
   # ...
)


def parse_better_js_alternative(code: Text) -> None:
    tokens: List = tokenize(code)
    syntax_tree: List = parse(tokens)

    for node in syntax_tree:
        pass


def tokenize(code: Text) -> List:
    statements = code.split()
    tokens: List[Dict] = []
    for regex in REGEXES:
        for statement in statements:
            pass

    return tokens


def parse(tokens: List) -> List:
    syntax_tree: List[Dict] = []
    for token in tokens:
        pass

    return syntax_tree
```


### Не используйте флаги как параметры функций

Флаги сообщают вашему пользователю, что эта функция соверашет несколько действий. Функции должны делать одно. Выделите под-функции, если они соответсвуют разным частям код на основе логической переменной.

<span style="color:red">**Bad**</span>

```python
from typing import Text
from tempfile import gettempdir
from pathlib import Path


def create_file(name: Text, temp: bool) -> None:
    if temp:
        (Path(gettempdir()) / name).touch()
    else:
```

<span style="color:green">**Good**</span>

```python
from typing import Text
from tempfile import gettempdir
from pathlib import Path


def create_file(name: Text) -> None:
    Path(name).touch()


def create_temp_file(name: Text) -> None:
    (Path(gettempdir()) / name).touch()
```

### Избегайте сайд-эффектов

Функция производит побочный эффект, если она делает что-либо, кроме как принимает значение в и
возвращает другое значение или значения. Побочным эффектом может быть запись в файл, изменение
какой-то глобальной переменной.

Главное - избегать распространенных ошибок, таких как разделение состояния между объектами.

<span style="color:red">**Bad**</span>

```python
# This is a module-level name.
# It's good practice to define these as immutable values, such as a string.
# However...
fullname = "Ryan McDermott"

def split_into_first_and_last_name() -> None:
    # The use of the global keyword here is changing the meaning of the
    # the following line. This function is now mutating the module-level
    # state and introducing a side-effect!
    global fullname
    fullname = fullname.split()

split_into_first_and_last_name()

# MyPy will spot the problem, complaining about 'Incompatible types in
# assignment: (expression has type "List[str]", variable has type "str")'
print(fullname)  # ["Ryan", "McDermott"]

# OK. It worked the first time, but what will happen if we call the
# function again?
```

<span style="color:green">**Good**</span>

```python
from typing import List, AnyStr


def split_into_first_and_last_name(name: AnyStr) -> List[AnyStr]:
    return name.split()

fullname = "Ryan McDermott"
name, surname = split_into_first_and_last_name(fullname)

print(name, surname)  # => Ryan McDermott
```

### Удаляйте неиспользуемый код

Мертвый код - это так же плохо, как и дублированный код. Нет причин держать это кодовой базе. Если его не называют, избавьтесь от него! Все сохраняется в git - и при желании его можно будет найти в истории.

<span style="color:red">**Bad**</span>

```python
from typing import Text

def old_request_module(url: Text)
    pass

def new_request_module(url: Text)
    pass

request = new_request_module(url)
InventoryTracker('apples', request, 'www.inventory-awesome.io')
```

<span style="color:green">**Good**</span>

```python
from typing import Text

def request_module(url: Text)
    pass

request = request_module(url)
InventoryTracker('apples', request, 'www.inventory-awesome.io')
```

## **Классы**

SOLID — это мнемоническая аббревиатура для набора принципов проектирования, созданных для разработки программного обеспечения при помощи объектно-ориентированных языков. Принципы **SOLID** направленны на содействие разработки более простого, надежного и обновляемого кода. Каждая буква в аббревиатуре **SOLID** соответствует одному принципу разработки.

При правильной реализации это делает ваш код более **расширяемым, логичным и легким для чтения**.

### S: Принцип единой ответственности (Single Responsibility Principle - SRP)

Принцип единственной обязанности требует того, чтобы **один класс выполнял только одну работу**. Таким образом, если у класса есть более одной работы, он становится зависимым. Изменение поведения одной работы класса приводит к изменению в другой.

<span style="color:red">**Bad**</span>

```python
# Below is Given a class which has two responsibilities
class  User:
    def __init__(self, name: str):
        self.name = name

    def get_name(self) -> str:
        pass

    def save(self, user: User):
        pass
```

Мы имеем класс User, который ответственен за две работы — свойства пользователя и управление базой данных. Если в приложении будет изменен функционал управления базой данных для пользователя, тогда классы использующие свойства класса User тоже придется доработать, чтобы компенсировать новые изменения. Это как домино эффект, уроните одну кость, и она уронит все за ней следом.

Мы же просто разделим класс. Мы создадим ещё один класс, который возьмет на себя одну ответственность — управление базой данных пользователя.

<span style="color:green">**Good**</span>

```python
class User:
    def __init__(self, name: str):
            self.name = name

    def get_name(self):
        pass


class UserDB:
    def get_user(self, id) -> User:
        pass

    def save(self, user: User):
        pass
```

### O: Принцип открытости/закрытости (Open/Closed Principle - OCP)

Программные сущности (**классы, модули, функции**) должно быть **открыты для расширения, но не модификации**.

Давайте представим, что у вас есть магазин, и вы даете скидку в 20% для ваших любимых покупателей используя класс Discount. Если бы вы решаете удвоить 20-ти процентную скидку для VIP клиентов, вы могли бы изменить класс следующим образом:

<span style="color:red">**Bad**</span>

```python
class Discount:
    def __init__(self, customer, price):
        self.customer = customer
        self.price = price

    def give_discount(self):
        if self.customer == 'fav':
            return self.price * 0.2
        if self.customer == 'vip':
            return self.price * 0.4
```

Но нет, это нарушает OCP. OCP запрещает это. Например, если мы хотим дать новую скидку для другого типа покупателей, то это требует добавления новой логики. Чтобы следовать OCP, мы добавим новый класс, который будет расширять Discount. И в этом новом классе реализуем требуемую логику:

<span style="color:green">**Good**</span>

```python
class Discount:
    def __init__(self, customer, price):
        self.customer = customer
        self.price = price

    def get_discount(self):
        return self.price * 0.2

class VIPDiscount(Discount):
    def get_discount(self):
        return super().get_discount() * 2
```

Если вы решите дать скидку супер VIP пользователям, то это будет выглядеть так:

```python
class SuperVIPDiscount(VIPDiscount):
    def get_discount(self):
      return super().get_discount() * 2
```

Расширяйте, но не модифицируйте.

### L: Принцип подстановки Барбары Лисков (Liskov Substitution Principle - LSP)

Главная идея, стоящая за Liskov Substitution Principle в том, что **для любого класса клиент должен иметь возможность использовать любой подкласс базового класса, не замечая разницы между ними**, и следовательно, без каких-либо изменений поведения программы при выполнении. Это означает, что клиент полностью изолирован и не подозревает об изменениях в иерархии классов.

*Более формально:
Пусть **q(x)** является свойством, верным относительно объектов **x** некоторого типа **T**. Тогда **q(y)** также должно быть верным для объектов **y** типа **S**, где **S** является подтипом типа **T**.*

Проще говоря, это значит, что подкласс, дочерний класс должны соответствовать их родительскому классу или супер классу.

<span style="color:red">**Bad**</span>
```python
def animal_leg_count(animals: list):
    for animal in animals:
        if isinstance(animal, Lion):
            print(lion_leg_count(animal))

        elif isinstance(animal, Mouse):
            print(mouse_leg_count(animal))

        elif isinstance(animal, Pigeon):
            print(pigeon_leg_count(animal))

animal_leg_count(animals)
```

<span style="color:green">**Good**</span>

```python
class Animal:
    def leg_count(self):
        pass


class Lion(Animal):
    def leg_count(self):
        pass


def animal_leg_count(animals: list):
    for animal in animals:
        print(animal.leg_count())

animal_leg_count(animals)
```

LSP это основа хорошего объектно-ориентированного проектирования программного обеспечения, потому что он следует одному из базовых принципов ООП — полиморфизму. Речь о том, чтобы создавать правильные иерархии, такие, что классы, производные от базового являлись полиморфными для их родителя по отношению к методам их интерфейсов.

Тщательное обдумывание новых классов в соответствии с LSP помогает нам расширять иерархию классов правильно. Также, LSP способствует OCP.

### I: Принцип разделения интерфейсов (Interface Segregation Principle - ISP)

Создавайте тонкие интерфейсы, которые ориентированы на клиента. **Клиенты не должны зависеть от интерфейсов, которые они не используют**. Этот принцип устраняет недостатки реализации больших интерфейсов.

Чтобы полностью проиллюстрировать это, мы возьмем классический пример, потому что он очень показательный и легок для понимания. Классический пример:

```python
class IShape:
    def draw(self):
        raise NotImplementedError

class Circle(IShape):
    def draw(self):
        pass

class Square(IShape):
    def draw(self):
        pass

class Rectangle(IShape):
    def draw(self):
        pass
```

Еще один приятный трюк заключается в том, что в нашей бизнес-логике отдельный класс может реализовать несколько интерфейсов, если необходимо. Таким образом, мы может предоставить единую реализацию для всех общих методов между интерфейсами. Сегрегированные интерфейсы заставляют нас больше думать о нашем коде с точки зрения клиента, что приведет нас к меньшей зависимости и более легкому тестированию. Таким образом, мы не только сделали наш код лучше для клиента, но также это облегчило нам понимание, тестирование и реализацию кода для нас самих.

### D: Принцип инверсии зависимостей (Dependency Inversion Principle - DIP)

**Зависимость должна быть от абстракций, а не от конкретной реализации**. Модули верхних уровней не должны зависеть от модулей нижних уровней. Классы и верхних, и нижних уровней должны зависеть от одних и тех же абстракций. Абстракции не должны зависеть от деталей. Детали должны зависеть от абстракций.

Наступает момент в разработке, когда наше приложение в основном состоит из модулей. Когда такое происходит, нам необходимо улучшать код используя внедрение зависимостей. Функционирование компонентов высокого уровня зависит от компонентов низкого уровня. Для создания определенного поведения вы можете использовать наследование или интерфейсы.

```python
class AuthenticationForUser:
    def __init__(self, connector: Connector):
        self.connection = connector.connect()

    def authenticate(self, credentials):
        pass

    def is_authenticated(self):
        pass

    def last_login(self):
        pass

class AnonymousAuth(AuthenticationForUser):
    pass

class GithubAuth(AuthenticationForUser):
    def last_login(self):
        pass

class FacebookAuth(AuthenticationForUser):
    pass

class Permissions:
    def __init__(self, auth: AuthenticationForUser):
        self.auth = auth

    def has_permissions():
        pass

class IsLoggedInPermissions(Permissions):
    def last_login():
        return auth.last_log
```

## **Don't repeat yourself (DRY)**

[DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself)

Делайте все возможное, чтобы избежать дублирования кода. Дублирование кода - это плохо, потому что
это означает, что есть более одного места, где нужно что-то поменять, если при измении некоторой логики.

Часто есть повторяющийся код, потому что у вас несколькой слегка отличающихся объекта, у которых много общего, но их различия заставляют вас
иметь две или несколько отдельных функций, выполняющих одни и те же действия. Удаление дублированног код означает создание правильной абстракции, которая может обрабатывать этот набор различных свойств, но только с одной функцией / модулем / классом.

Правильная абстракция имеет решающее значение. Плохие абстракции могут быть
хуже, чем дублированный код, так что будьте осторожны!

<span style="color:red">**Bad**</span>

```python
from typing import List, Text, Dict
from dataclasses import dataclass

@dataclass
class Developer:
    def __init__(self, experience: float, github_link: Text) -> None:
        self._experience = experience
        self._github_link = github_link

    @property
    def experience(self) -> float:
        return self._experience

    @property
    def github_link(self) -> Text:
        return self._github_link

@dataclass
class Manager:
    def __init__(self, experience: float, github_link: Text) -> None:
        self._experience = experience
        self._github_link = github_link

    @property
    def experience(self) -> float:
        return self._experience

    @property
    def github_link(self) -> Text:
        return self._github_link


def get_developer_list(developers: List[Developer]) -> List[Dict]:
    developers_list = []
    for developer in developers:
        developers_list.append({
            'experience' : developer.experience,
            'github_link' : developer.github_link
        })
    return developers_list

def get_manager_list(managers: List[Manager]) -> List[Dict]:
    managers_list = []
    for manager in managers:
        managers_list.append({
            'experience' : manager.experience,
            'github_link' : manager.github_link
        })
    return managers_list

## create list objects of developers
company_developers = [
    Developer(experience=2.5, github_link='https://github.com/1'),
    Developer(experience=1.5, github_link='https://github.com/2')
]
company_developers_list = get_developer_list(developers=company_developers)

## create list objects of managers
company_managers = [
    Manager(experience=4.5, github_link='https://github.com/3'),
    Manager(experience=5.7, github_link='https://github.com/4')
]
company_managers_list = get_manager_list(managers=company_managers)
```

<span style="color:green">**Good**</span>

```python
from typing import List, Text, Dict
from dataclasses import dataclass

@dataclass
class Employee:
    def __init__(self, experience: float, github_link: Text) -> None:
        self._experience = experience
        self._github_link = github_link

    @property
    def experience(self) -> float:
        return self._experience

    @property
    def github_link(self) -> Text:
        return self._github_link



def get_employee_list(employees: List[Employee]) -> List[Dict]:
    employees_list = []
    for employee in employees:
        employees_list.append({
            'experience' : employee.experience,
            'github_link' : employee.github_link
        })
    return employees_list

## create list objects of developers
company_developers = [
    Employee(experience=2.5, github_link='https://github.com/1'),
    Employee(experience=1.5, github_link='https://github.com/2')
]
company_developers_list = get_employee_list(employees=company_developers)

## create list objects of managers
company_managers = [
    Employee(experience=4.5, github_link='https://github.com/3'),
    Employee(experience=5.7, github_link='https://github.com/4')
]
company_managers_list = get_employee_list(employees=company_managers)
```
